(function () {
  'use strict';
  angular
    .module('pumpwoodAngular', ['ngResource']);

  angular
    .module('pumpwoodAngular')
    .config(function ($resourceProvider) {
      $resourceProvider.defaults.stripTrailingSlashes = false;
    })
    .constant('AUTH_EVENTS', {
      loginSuccess: 'auth-login-success',
      loginFailed: 'auth-login-failed',
      logoutSuccess: 'auth-logout-success',
      sessionTimeout: 'auth-session-timeout',
      notAuthenticated: 'auth-not-authenticated',
      notAuthorized: 'auth-not-authorized'
    });
})();
(function () {
  'use strict';

  angular
    .module('pumpwoodAngular')
    .service('pumpwoodApi', pumpwoodApi);

  function pumpwoodApi($resource, SERVER_CONFIG, pumpwoodAuth) {
    var serviceURL = SERVER_CONFIG.URL + '/rest/';

    this.getRequestObject = function (modelClass) {
      return $resource(serviceURL + ':modelClass/:alias/:pk/',
        {
          modelClass: modelClass,
          alias: '@alias'
        },
        {
          retrieve: {
            method: 'GET',
            params: { alias: 'retrieve' },
            headers: pumpwoodAuth.getHeaders()
          },
          list: {
            method: 'POST',
            isArray: true,
            params: { alias: 'list' },
            headers: pumpwoodAuth.getHeaders()
          },
          listWithoutPag: {
            method: 'POST',
            isArray: true,
            params: { alias: 'list-without-pag' },
            headers: pumpwoodAuth.getHeaders()
          },
          save: {
            method: 'POST',
            params: { alias: 'save' },
            headers: pumpwoodAuth.getHeaders()
          },
          getActions: {
            method: 'GET',
            params: { alias: 'actions' },
            headers: pumpwoodAuth.getHeaders()
          },
          postActions: {
            method: 'POST',
            params: { alias: 'actions/:action/:pk/' },
            headers: pumpwoodAuth.getHeaders()
          },
          getSearchOptions: {
            method: 'GET',
            params: { alias: 'options' },
            headers: pumpwoodAuth.getHeaders()
          },
          postOptions: {
            method: 'POST',
            params: { alias: 'options' },
            headers: pumpwoodAuth.getHeaders()
          },
          pivot: {
            method: 'POST',
            params: { alias: 'pivot'},
            headers: pumpwoodAuth.getHeaders()
          },
          pivotAsArray: {
            method: 'POST',
            params: { alias: 'pivot', isArray:true},
            headers: pumpwoodAuth.getHeaders()
          }

        });
    };
  }

} ());
(function() {
  'use strict';

})();

(function () {
  'use strict';

  angular
    .module('pumpwoodAngular')
    .service('pumpwoodAuth', pumpwoodAuth);

  function pumpwoodAuth($rootScope, $window, $resource, SERVER_CONFIG) {
    function getHeaders() {
      return !$window.localStorage.pumpwoodAuthorization ? {} :
        {
          'Authorization': $window.localStorage.pumpwoodAuthorization,
        };
    }

    function getResource() {
      return $resource(SERVER_CONFIG.URL + '/rest/registration/:alias/:uid/:key',
        {
          alias: '@alias'
        },
        {
          login: {
            method: 'POST',
            cache: true,
            params: { alias: 'login' }
          },
          logout: {
            method: 'GET',
            cache: true,
            params: { alias: 'logout' },
            headers: getHeaders()
          },
          signup: {
            method: 'POST',
            cache: true,
            params: { alias: 'signup' }
          },
          activateUser: {
            method: 'GET',
            cache: true,
            params: { alias: 'activate' }
          },
          newActivationKey: {
            method: 'GET',
            cache: true,
            params: { alias: 'newactivationlink' }
          },
          resetPassword: {
            method: 'POST',
            cache: true,
            params: { alias: 'resetpassword' }
          },
          resetPasswordConfirm: {
            method: 'POST',
            cache: true,
            params: { alias: 'resetpasswordconfirm' }
          },
          changePassword: {
            method: 'POST',
            cache: true,
            params: { alias: 'changepassword' },
            headers: getHeaders()
          }
        }
      );
    }

    function login(userName, password, callback, failCallback) {
      getResource().login({}, { username: userName, password: password },
        function (data) {
          $window.localStorage.pumpwoodAuthorization = data.token;
          if (callback) callback(data);
        },
        function (response) {
          if (failCallback) failCallback(response);
        });
    }

    function logout(callback, failCallback) {
      getResource().logout(function (data) {
        $window.localStorage.removeItem("pumpwoodAuthorization");
        if (callback) callback(data);
      }, function (response) {
        if (failCallback) failCallback(response);
      });
    }

    function signup(signupInfo, callback, failCallback) {
      getResource().signup({}, signupInfo,
        function (response) {
          if (callback) callback(response);
        }, function (response) {
          if (failCallback) failCallback(response);
        });
    }

    function activateUser(activationKey, callback, failCallback) {
      getResource().activateUser({ key: activationKey },
        function (response) {
          if (callback) callback(response);
        }, function (response) {
          if (failCallback) failCallback(response);
        });
    }

    function newActivationKey(userId, callback, failCallback) {
      getResource().newActivationKey({ uid: userId },
        function (response) {
          if (callback) callback(response);
        }, function (response) {
          if (failCallback) failCallback(response);
        });
    }

    function resetPassword(username, callback, failCallback) {
      getResource().resetPassword({}, { username: username },
        function (response) {
          if (callback) callback(response);
        }, function (response) {
          if (failCallback) failCallback(response);
        });
    }

    function resetPasswordConfirm(resetKey, password, callback, failCallback) {
      getResource().resetPasswordConfirm({ key: resetKey }, { password: password },
        function (response) {
          if (callback) callback(response);
        }, function (response) {
          if (failCallback) failCallback(response);
        });
    }
    
    function changePassword(password, newPassword, callback, failCallback) {
      getResource().changePassword({}, { password: password , new_password: newPassword },
        function (response) {
          if (callback) callback(response);
        }, function (response) {
          if (failCallback) failCallback(response);
        });
    }

    function retrieveLogged(callback, failCallback) {
      $resource(SERVER_CONFIG.URL + '/rest/registration/retrieveauthenticateduser/', {},
        {
          retrieveLogged: {
            method: 'GET',
            cache: true,
            params: {},
            headers: (function() {
              var headers = getHeaders();
              headers['Authorization'] = 'Token ' + headers['Authorization'];
              return headers;
            }
           )()
          }
        }).retrieveLogged(function(response) {
          if (callback) callback(response);
        }, function(response) {
          if (failCallback) failCallback(response);
        });
    }

    return {
      getHeaders: getHeaders,
      login: login,
      logout: logout,
      signup: signup,
      activateUser: activateUser,
      newActivationKey: newActivationKey,
      resetPassword: resetPassword,
      resetPasswordConfirm: resetPasswordConfirm,
      changePassword: changePassword,
      retrieveLogged: retrieveLogged,
    };
  }

} ());
(function() {
  'use strict';

})();
