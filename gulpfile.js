var gulp = require('gulp');
var eslint = require('gulp-eslint');
var size = require('gulp-size');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');

var del = require('del');

var prodFileName = 'pumpwood-angular';

gulp.task('lint', function() {
  return gulp.src(['src/**/*.js', 'test/**/*_spec.js'])
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(size());
});

gulp.task('scripts-dev', function () {
    return gulp.src('src/**/*.js')
        .pipe(concat(prodFileName + '.js'))
        .pipe(gulp.dest('gen'));
});

gulp.task('scripts-prod', function () {
    return gulp.src('src/**/*.js')
        .pipe(concat(prodFileName + '.js'))
        .pipe(gulp.dest('dist'))
        .pipe(uglify())
        .pipe(rename(prodFileName + '.min.js'))
        .pipe(gulp.dest('dist'));
});

gulp.task('watch', function() {
  gulp.watch('src/**/*.js', ['lint', 'scripts-dev']);
});

gulp.task('clean', function (cb) {
    del(['dist', 'gen'], cb);
});

gulp.task('default', ['lint', 'scripts-dev', 'watch']);
gulp.task('production', ['lint', 'scripts-prod']);
