(function () {
  'use strict';
  angular
    .module('pumpwoodAngular', ['ngResource']);

  angular
    .module('pumpwoodAngular')
    .config(function ($resourceProvider) {
      $resourceProvider.defaults.stripTrailingSlashes = false;
    })
    .constant('AUTH_EVENTS', {
      loginSuccess: 'auth-login-success',
      loginFailed: 'auth-login-failed',
      logoutSuccess: 'auth-logout-success',
      sessionTimeout: 'auth-session-timeout',
      notAuthenticated: 'auth-not-authenticated',
      notAuthorized: 'auth-not-authorized'
    });
})();