(function () {
  'use strict';

  angular
    .module('pumpwoodAngular')
    .service('pumpwoodApi', pumpwoodApi);

  function pumpwoodApi($resource, SERVER_CONFIG, pumpwoodAuth) {
    var serviceURL = SERVER_CONFIG.URL + '/rest/';

    this.getRequestObject = function (modelClass) {
      return $resource(serviceURL + ':modelClass/:alias/:pk/',
        {
          modelClass: modelClass,
          alias: '@alias'
        },
        {
          retrieve: {
            method: 'GET',
            params: { alias: 'retrieve' },
            headers: pumpwoodAuth.getHeaders()
          },
          list: {
            method: 'POST',
            isArray: true,
            params: { alias: 'list' },
            headers: pumpwoodAuth.getHeaders()
          },
          listWithoutPag: {
            method: 'POST',
            isArray: true,
            params: { alias: 'list-without-pag' },
            headers: pumpwoodAuth.getHeaders()
          },
          save: {
            method: 'POST',
            params: { alias: 'save' },
            headers: pumpwoodAuth.getHeaders()
          },
          getActions: {
            method: 'GET',
            params: { alias: 'actions' },
            headers: pumpwoodAuth.getHeaders()
          },
          postActions: {
            method: 'POST',
            params: { alias: 'actions/:action/:pk/' },
            headers: pumpwoodAuth.getHeaders()
          },
          getSearchOptions: {
            method: 'GET',
            params: { alias: 'options' },
            headers: pumpwoodAuth.getHeaders()
          },
          postOptions: {
            method: 'POST',
            params: { alias: 'options' },
            headers: pumpwoodAuth.getHeaders()
          },
          pivot: {
            method: 'POST',
            params: { alias: 'pivot'},
            headers: pumpwoodAuth.getHeaders()
          },
          pivotAsArray: {
            method: 'POST',
            params: { alias: 'pivot', isArray:true},
            headers: pumpwoodAuth.getHeaders()
          }

        });
    };
  }

} ());